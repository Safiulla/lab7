#include <iostream>
#include <string.h>
using namespace std;

void reverse(char sample[]){
  int arrSize = strlen(sample);
  for(int i=0; i<arrSize/2; i++){
    swap(sample[i], sample[(arrSize-1)-i]);
  }
}

int main() {
  char sample[] = "Harry";
  int arrSize = strlen(sample);
  
  cout<< "Before: ";
  for (int i=0; i < arrSize; i++)
    std::cout << sample[i];

  cout<< endl;
  cout<< "After: ";

  reverse(sample);
  for (int i=0; i < arrSize; i++)
    std::cout << sample[i];
}
