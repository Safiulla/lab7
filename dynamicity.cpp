#include <iostream>
#include <string.h>
#include <stdio.h>
using namespace std;

int main() {
  struct Employee{
    string firstName;
    string lastName;
    string ID;
    string email;
    string phoneNumber;
    double salary;
    Employee *manager;
  };

  int size;
  cout << "Enter the number of staff: ";
  cin >> size;

  Employee *members;
  members = new Employee[size];

  for(int i=0; i<size; i++){
    cout << "Enter the first name of employee " << (i+1) <<": ";
    cin >> members[i].firstName;
    cout << "Enter the last name of employee " << (i+1) <<": ";
    cin >> members[i].lastName;
    cout << "Enter the ID of employee " << (i+1) <<": ";
    cin >> members[i].ID;
    cout << "Enter the email of employee " << (i+1) <<": ";
    cin >> members[i].email;
    cout << "Enter the phone number of employee " << (i+1) <<": ";
    cin >> members[i].phoneNumber;
    cout << "Enter the salary of employee " << (i+1) <<": ";
    cin >> members[i].salary;
    cout << endl;
  }
  cout << endl;
  for(int i=0; i<size; i++){
    int sentence = 20-(members[i].firstName.size()+members[i].lastName.size()+1);
    cout << "Name: " << members[i].firstName +" "+ members[i].lastName;

    for(int u=0; u<sentence; u++)
      cout<<" ";
    
    cout << "   ID: " << members[i].ID << endl;
  }
  delete [] members;
}
