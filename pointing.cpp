#include <iostream>
using namespace std;

int main() {
  double sample = 2.4;
  double *point = &sample;

  std::cout << "Variable value: " << sample << "; address: " << point << endl;
  std::cout << "Pointer value: " << point << "; address: " << &point << "; dereference: " << *point << endl;
}
