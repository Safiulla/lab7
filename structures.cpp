#include <iostream>
#include <string.h>
using namespace std;

int main() {
  struct Employee{
    string firstName;
    string lastName;
    string ID;
    string email;
    string phoneNumber;
    double salary;
    Employee *manager;
  };

  Employee* management = new Employee;
  management->firstName = "Ifas"; management->lastName = "pseudonym"; management->ID = "1045"; management->email = "Ifas@bleepasaurus.com"; management->phoneNumber = "099-9595933";
  management->salary = 40000;

  Employee Staff1; Staff1.firstName = "Mark"; Staff1.lastName = "Talin"; Staff1.ID = "2040"; Staff1.email = "mark.talin@bleepasaurus.com"; Staff1.phoneNumber = "099-100012921"; Staff1.salary = 15000; Staff1.manager = management;

  Employee Staff2; Staff2.firstName = "Jack"; Staff2.lastName = "Dutler"; Staff2.ID = "2041"; Staff2.email = "jack.dutler@bleepasaurus.com"; Staff2.phoneNumber = "099-100012922"; Staff2.salary = 15000; Staff2.manager = management;

  cout << "Employees \n"; 
  cout << "Name: " + Staff1.firstName + " " + Staff1.lastName << "   ID: " + Staff1.ID << "\nEmail: " + Staff1.email << "    Phone number: " + Staff1.phoneNumber + "\nSalary: " << Staff1.salary << "\nManager details: " + (*Staff1.manager).firstName+" "+(*Staff2.manager).lastName + "   " + "id: "+(*Staff1.manager).ID + "\n\n";

  cout << "Name: " + Staff2.firstName + " " + Staff2.lastName << "   ID: " + Staff2.ID << "\nEmail: " + Staff2.email << "    Phone number: " + Staff2.phoneNumber + "\nSalary: " << Staff2.salary << "\nManager details: " + (*Staff2.manager).firstName+" "+(*Staff2.manager).lastName + "   " + "id: "+(*Staff2.manager).ID + "\n\n";

  cout << "Manager \n"; 
  cout << "Name: " + management->firstName + " " + management->lastName << "   ID: " + management->ID << "\nEmail: " + management->email << "    Phone number: " + management->phoneNumber + "\nSalary: " << management->salary << endl;
}
